#pragma once

#include <stb_truetype.h>
#include <vector>
#include <cstdio>
#include <fstream>
#include <string>

#include "Vec2.h"

struct Font {

	static Font *load(const char* filename, float height);
	float         getHeight() const { return height; }
	Vec2          getTextSize(const char* text) const;
	void          draw(const char* text, const Vec2& pos) const;

	float height;
	ltex_t *texture;
	std::vector<stbtt_bakedchar> charData;

	~Font() { delete texture; }

private:
	Font(float _height, ltex_t *_texture, std::vector<stbtt_bakedchar> _charData) { height = _height; texture = _texture; charData = _charData; }
	
};

inline Font* Font::load(const char* filename, float height) {
	size_t fsize;
	size_t size = 128;
	FILE* fontFile = fopen(filename, "rb");
	std::vector<unsigned char> pixels(size*size);
	std::vector<stbtt_bakedchar> charD = std::vector<stbtt_bakedchar>(96);

	if (fontFile == NULL) { fputs("File error", stderr); return 0; }
	
	fseek(fontFile, 0, SEEK_END);
	fsize = ftell(fontFile);
	fseek(fontFile, 0, SEEK_SET);

	std::vector<unsigned char> fontBuffer(fsize);
	fread_s(fontBuffer.data(), fsize, 1, fsize, fontFile);
	fclose(fontFile);

	while (stbtt_BakeFontBitmap(fontBuffer.data(), 0, height, pixels.data(), size, size, 32, 96, (charD).data()) <= 0) {
		size *= 2;
		pixels.resize(size*size);
	}

	std::vector<unsigned char> colorBuffer;
	int r = rand() % 256;
	int g = rand() % 256;
	int b = rand() % 256;
	for (int i = 0; i < pixels.size(); i++) {
		colorBuffer.push_back(r);
		colorBuffer.push_back(g);
		colorBuffer.push_back(b);
		colorBuffer.push_back(pixels[i]);
	}

	ltex_t* tex = ltex_alloc(size, size, 0);
	ltex_setpixels(tex, colorBuffer.data());

	Font *f = new Font(height, tex, charD);

	pixels.clear();
	colorBuffer.clear();
	fontBuffer.clear();

	return f;
}

void Font::draw(const char *text, const Vec2 &pos) const {
	lgfx_setblend(BLEND_ALPHA);
	stbtt_aligned_quad *aligned_quad = new stbtt_aligned_quad();
	float xPos = pos.x;
	float yPos = pos.y;
	for (int i = 0; i < strlen(text); i++) {
		int carIndex = text[i] - 32;
		stbtt_GetBakedQuad(charData.data(), texture->width, texture->height, carIndex, &xPos, &yPos, aligned_quad, true);
		ltex_drawrotsized(texture, aligned_quad->x0, aligned_quad->y0, 0, 0, 0, aligned_quad->x1 - aligned_quad->x0, aligned_quad->y1 - aligned_quad->y0, aligned_quad->s0, aligned_quad->t0, aligned_quad->s1, aligned_quad->t1);
	}

	delete aligned_quad;
}