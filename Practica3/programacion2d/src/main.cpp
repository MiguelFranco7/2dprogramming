#ifdef _MSC_VER
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif

#define _CRT_SECURE_NO_DEPRECATE
#define LITE_GFX_IMPLEMENTATION
#define STB_TRUETYPE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION

#include <litegfx.h>
#include <stb_image.h>
#include "stringutils.h"
#include "imageUtils.h"
#include "Font.h"
#include <glfw3.h>
#include <iostream>
#include <ctime>

using namespace std;

int main() {

	 // PRACTICA 3
	// Inicializamos GLFW
	if (!glfwInit()) {
		cout << "Error: No se ha podido inicializar GLFW" << endl;
		return -1;
	}
	atexit(glfwTerminate);

	// Creamos la ventana
	glfwWindowHint(GLFW_RESIZABLE, true);
	GLFWwindow* window = glfwCreateWindow(800, 600, "Practica 3 Miguel Franco Medina", nullptr, nullptr);
	if (!window) {
		cout << "Error: No se ha podido crear la ventana" << endl;
		return -1;
	}

	// Activamos contexto de OpenGL
	glfwMakeContextCurrent(window);

	// Inicializamos LiteGFX
	lgfx_setup2d(800, 600);

	Font *font1 = (*font1).load("data/ArmyStamp.ttf", 20);
	Font *font2 = (*font2).load("data/BeautyDemo.ttf", 80);
	Font *font3 = (*font3).load("data/CFNightmare.ttf", 32);
	Font *font4 = (*font3).load("data/FunSized.ttf", 32);

	struct FontControl {
		Font *font;
		int vel;
		Vec2 pos;
	};
	std::vector<const char *> ttf;
	std::vector<FontControl> fonts;

	ttf.push_back("data/ArmyStamp.ttf");
	ttf.push_back("data/BeautyDemo.ttf");
	ttf.push_back("data/CFNightmare.ttf");
	ttf.push_back("data/FunSized.ttf");

	// Bucle principal
	const char *text = "Hello, world!";
	double lastTime = glfwGetTime();
	srand(time(0));

	while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {

		/*for (auto o : list) {}
		std::remove_if() usando lambda*/


		// Actualizamos delta
		float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
		lastTime = glfwGetTime();

		// Actualizamos tama�o de ventana
		int screenWidth, screenHeight;
		glfwGetWindowSize(window, &screenWidth, &screenHeight);
		lgfx_setviewport(0, 0, screenWidth, screenHeight);

		lgfx_setblend(BLEND_SOLID);
		ltex_t* tex = loadTexture("data/wall.jpg");
		ltex_drawrotsized(tex, 0, 0, 0, 0, 0, static_cast<float>(screenWidth), static_cast<float>(screenHeight), 0, 0, static_cast<float>(screenWidth) / static_cast<float>(tex->width), static_cast<float>(screenHeight) / static_cast<float>(tex->height));
		delete tex;
		// Actualizado de la l�gica del programa
		if (rand() % 101 < 1) { // Nuevo texto en pantalla.
			FontControl font;
			Font *f;
			font.font= (*f).load(ttf.at(rand() % ttf.size()), rand() % 101);
			font.pos = Vec2(screenWidth, rand() % screenHeight);
			font.vel = rand() % 201 + 20;

			fonts.push_back(font);
		}

		for (int i = 0; i < fonts.size(); i++) {
			fonts.at(i).pos.x -= deltaTime * fonts.at(i).vel;
			fonts.at(i).font->draw(text, fonts.at(i).pos);
			if (fonts.at(i).pos.x < 0) {
				/*delete fonts.at(i).font;
				i--;*/
				printf("");
			}

			stbtt_aligned_quad *aligned_quad = new stbtt_aligned_quad();
			float xPos = fonts.at(i).pos.x;
			float yPos = fonts.at(i).pos.y;
			int carIndex = 1;//;text[13] - 32;
			stbtt_GetBakedQuad(fonts[i].font->charData.data(), fonts.at(i).font->texture->width, fonts.at(i).font->texture->height, carIndex, &xPos, &yPos, aligned_quad, true);
			delete aligned_quad;

			if (xPos <= 0) {
				carIndex;
				printf("");
			}
		}

		// Actualizamos ventana y eventos
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// PRACTICA 2
	//// Inicializamos GLFW
	//if (!glfwInit()) {
	//	cout << "Error: No se ha podido inicializar GLFW" << endl;
	//	return -1;
	//}
	//atexit(glfwTerminate);

	//// Creamos la ventana
	//glfwWindowHint(GLFW_RESIZABLE, true);
	//GLFWwindow* window = glfwCreateWindow(800, 600, "Practica 2 Miguel Franco Medina", nullptr, nullptr);
	//if (!window) {
	//	cout << "Error: No se ha podido crear la ventana" << endl;
	//	return -1;
	//}

	//// Activamos contexto de OpenGL
	//glfwMakeContextCurrent(window);

	//// Inicializamos LiteGFX
	//lgfx_setup2d(800, 600);

	//// Bucle principal
	//float angle = 0;
	//float velAngle = 10;
	//float escalado = 1; // 0.8 - 1.2 a 0.5 por seg.
	//float velEscalado = 0.5;
	//double lastTime = glfwGetTime();
	//int operAngle = 1;
	//int operEsc = 1;
	//while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {

	//	// Actualizamos delta
	//	float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
	//	lastTime = glfwGetTime();

	//	// Actualizamos tama�o de ventana
	//	int screenWidth, screenHeight;
	//	glfwGetWindowSize(window, &screenWidth, &screenHeight);
	//	lgfx_setviewport(0, 0, screenWidth, screenHeight);

	//	// Actualizamos coordenadas de raton
	//	double mouseX, mouseY;
	//	glfwGetCursorPos(window, &mouseX, &mouseY);

	//	// Actualizado de la l�gica del programa
	//	if (operAngle == 1) angle += velAngle * deltaTime;
	//	else angle -= velAngle * deltaTime;

	//	if (angle >= 10) operAngle = -1;
	//	else if (angle <= -10) operAngle = 1;

	//	if (operEsc == 1) escalado += velEscalado * deltaTime;
	//	else escalado -= velEscalado * deltaTime;

	//	if (escalado >= 1.2) operEsc = -1;
	//	else if (escalado <= 0.8) operEsc = 1;

	//	// Pintado
	//	lgfx_setblend(BLEND_SOLID);
	//	ltex_t* tex = loadTexture("data/wall.jpg");
	//	ltex_drawrotsized(tex, 0, 0, 0, 0, 0, static_cast<float>(screenWidth), static_cast<float>(screenHeight), 0, 0, static_cast<float>(screenWidth) / static_cast<float>(tex->width), static_cast<float>(screenHeight) / static_cast<float>(tex->height));
	//	
	//	float pivotX = 0.5f;
	//	float pivotY = 0.7f;
	//	lgfx_setblend(BLEND_ADD);
	//	tex = loadTexture("data/fire.png");
	//	ltex_drawrotsized(tex, static_cast<float>(mouseX), static_cast<float>(mouseY), angle, pivotX, pivotY, tex->width*escalado, tex->height*escalado, 0, 0, 1, 1);

	//	lgfx_setblend(BLEND_ALPHA);
	//	tex = loadTexture("data/grille.png");
	//	ltex_drawrotsized(tex, 0, 0, 0, 0, 0, static_cast<float>(screenWidth), static_cast<float>(screenHeight), 0, 0, static_cast<float>(screenWidth) / static_cast<float>(tex->width), static_cast<float>(screenHeight) / static_cast<float>(tex->height));

	//	lgfx_setblend(BLEND_MUL);
	//	tex = loadTexture("data/light.png");
	//	ltex_drawrotsized(tex, static_cast<float>(mouseX - tex->width / 2), static_cast<float>(mouseY - tex->height / 2), 0, 0, 0, static_cast<float>(tex->width), static_cast<float>(tex->height), 0, 0, 1, 1);

	//	lgfx_setcolor(0, 0, 0, 0);
	//	lgfx_drawrect(0, 0, static_cast<float>(screenWidth), static_cast<float>(mouseY - tex->height / 2));
	//	lgfx_drawrect(0, 0, static_cast<float>(mouseX - tex->height / 2), static_cast<float>(screenHeight));
	//	lgfx_drawrect(static_cast<float>(mouseX + tex->height / 2), 0, static_cast<float>(screenWidth - mouseX + tex->width / 2), static_cast<float>(screenHeight));
	//	lgfx_drawrect(0, static_cast<float>(mouseY + tex->height / 2), static_cast<float>(screenWidth), static_cast<float>(screenHeight - mouseY + tex->height/ 2));
	//	lgfx_setcolor(1, 1, 1, 1);

	//	// Actualizamos ventana y eventos
	//	glfwSwapBuffers(window);
	//	glfwPollEvents();

	//	deleteTexture(tex);
	//}

	// PRACTICA 1
	//// Inicializamos GLFW
	//if (!glfwInit()) {
	//	cout << "Error: No se ha podido inicializar GLFW" << endl;
	//	return -1;
	//}
	//atexit(glfwTerminate);

	//// Creamos la ventana
	//glfwWindowHint(GLFW_RESIZABLE, true);
	//GLFWwindow* window = glfwCreateWindow(800, 600, "Practica 1", nullptr, nullptr);
	//if (!window) {
	//	cout << "Error: No se ha podido crear la ventana" << endl;
	//	return -1;
	//}

	//// Activamos contexto de OpenGL
	//glfwMakeContextCurrent(window);

	//// Inicializamos LiteGFX
	//lgfx_setup2d(800, 600);

	//// Bucle principal
	//float angle = 0;
	//double lastTime = glfwGetTime();
	//while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {
	//	// Actualizamos delta
	//	float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
	//	lastTime = glfwGetTime();

	//	// Actualizamos tama�o de ventana
	//	int screenWidth, screenHeight;
	//	glfwGetWindowSize(window, &screenWidth, &screenHeight);
	//	lgfx_setviewport(0, 0, screenWidth, screenHeight);

	//	// Actualizamos coordenadas de raton
	//	double mouseX, mouseY;
	//	glfwGetCursorPos(window, &mouseX, &mouseY);

	//	// Actualizacion de logica del programa
	//	angle += 32 * deltaTime;
	//	float radAngle = angle * DEG2RAD;
	//	Vec2 size(32, 32);
	//	Vec2 center = Vec2(static_cast<float>(screenWidth), static_cast<float>(screenHeight)) / 2;
	//	Vec2 mousePos(static_cast<float>(mouseX), static_cast<float>(mouseY));
	//	Vec2 circle = mousePos + Vec2(cos(radAngle), -sin(radAngle)) * 64;
	//	Vec2 halfSize = size / 2;
	//	Vec2 centerOrig = center - halfSize;
	//	Vec2 mouseOrig = mousePos - halfSize;
	//	Vec2 circleOrig = circle - halfSize;

	//	// Pintado
	//	lgfx_clearcolorbuffer(1, 1, 1);
	//	lgfx_setcolor(1, 0, 0, 1);
	//	lgfx_drawrect(centerOrig.x, centerOrig.y, size.x, size.y);
	//	lgfx_setcolor(0, 1, 0, 1);
	//	lgfx_drawrect(mouseOrig.x, mouseOrig.y, size.x, size.y);
	//	lgfx_setcolor(0, 0, 1, 1);
	//	lgfx_drawoval(circleOrig.x, circleOrig.y, size.x, size.y);

	//	// Actualizamos ventana
	//	std::string title = "Distancia: "
	//		+ stringFromNumber<int>(mousePos.distance(center))
	//		+ " -- Angulo: "
	//		+ stringFromNumber<int>(mousePos.angle(circle));
	//	glfwSetWindowTitle(window, title.c_str());

	//	// Actualizamos ventana y eventos
	//	glfwSwapBuffers(window);
	//	glfwPollEvents();
	//}

    return 0;
}