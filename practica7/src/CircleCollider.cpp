#include "CircleCollider.h"
#include "ColliderUtils.h"

CircleCollider::CircleCollider(Vec2 *pos, Vec2 siz, Vec2 *sca) {
	position = pos;
	size = siz;
	scale = sca;
}

bool CircleCollider::collides(const Collider& other) const {
	return other.collides(*position, getRadius());
}

bool CircleCollider::collides(const Vec2& circlePos, float circleRadius) const {
	return checkCircleCircle(*position, getRadius(), circlePos, circleRadius);
}

bool CircleCollider::collides(const Vec2& rectPos, const Vec2& rectSize) const {
	return checkCircleRect(*position, getRadius(), rectPos, rectSize);
}

bool CircleCollider::collides(const Vec2& pixelsPos, const Vec2& pixelsSize, const uint8_t* pixels) const {
	return checkCirclePixels(*position, getRadius(), pixelsPos, pixelsSize, pixels);
}

float CircleCollider::getRadius() const {
	return (size.x > size.y) ? size.x * (*scale).x : size.y * (*scale).x;
}