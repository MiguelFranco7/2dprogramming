#pragma once
#include <iostream>
#include "Collider.h"
#include "Vec2.h"

class RectCollider : public Collider {
public:
	RectCollider(Vec2 *position, Vec2 size, Vec2 *scale, Vec2 *pivot);
	virtual bool collides(const Collider& other) const;
	virtual bool collides(const Vec2& circlePos, float circleRadius) const;
	virtual bool collides(const Vec2& rectPos, const Vec2& rectSize) const;
	virtual bool collides(const Vec2& pixelsPos, const Vec2& pixelsSize,
		const uint8_t* pixels) const;

	Vec2 *position;
	Vec2  size;
	Vec2 *scale;
	Vec2 *pivot;

private:
	Vec2 getSizeCollider() const;
	Vec2 getPositionCollider() const;
};