#pragma once
#include "ugine.h"
#include "vec2.h"
#include "sprite.h"
#include "pugi\pugixml.hpp"

using namespace pugi;

class World {
public:

	World(
		float clearRed = 0.15f, float clearGreen = 0.15f, float clearBlue = 0.15f,
		const ltex_t* back = nullptr) :
		mColorClear{ clearRed, clearGreen, clearBlue },
		mBacks{ back },
		mRatiosScrollBacks{ 0 },
		mVelScrollBacks{ 0 },
		mBacksPos{ 0 },
		mCameraPos(0, 0) {}

	~World() {
		for (size_t i = 0; i < mListSprites.size(); i++) {
			mListSprites.erase(mListSprites.begin() + i);
		}

		for (size_t i = 0; i < mSpritesTile.size(); i++) {
			mSpritesTile.erase(mSpritesTile.begin() + i);
		}
	}

	float getClearRed	() const		   { return mColorClear[0]; }
	void  setClearRed	(float clearRed)   { mColorClear[0] = clearRed; }
	float getClearGreen () const		   { return mColorClear[1]; }
	void  setClearGreen (float clearGreen) { mColorClear[1] = clearGreen; }
	float getClearBlue  () const		   { return mColorClear[2]; }
	void  setClearBlue  (float clearBlue)  { mColorClear[2] = clearBlue; }

	const ltex_t* getBackground(size_t layer) const { return mBacks[layer]; }
	void setBackground(size_t layer, ltex_t* back) { mBacks[layer] = back; }

	float getScrollRatio(size_t layer) const { return mRatiosScrollBacks[layer]; }
	void setScrollRatio(size_t layer, float ratio) { mRatiosScrollBacks[layer] = ratio; }

	const Vec2& getScrollSpeed(size_t layer) const { return mVelScrollBacks[layer]; }
	void setScrollSpeed(size_t layer, const Vec2& speed) { mVelScrollBacks[layer] = speed; }

	const Vec2& getCameraPosition() const { return mCameraPos; }
	void setCameraPosition(const Vec2& pos) { mCameraPos = pos; }

	void addSprite(Sprite& sprite) { mListSprites.push_back(&sprite); }
	void removeSprite(Sprite& sprite) {
		for (size_t i = 0; i <= mListSprites.size(); i++) {
			if (&sprite == mListSprites[i]) {
				mListSprites.erase(mListSprites.begin() + i);
			}
		}

		for (size_t i = 0; i <= mSpritesTile.size(); i++) {
			if (&sprite == mSpritesTile[i]) {
				mSpritesTile.erase(mSpritesTile.begin() + i);
			}
		}
	}
	
	void update(float deltaTime);
	void draw(const Vec2& screenSize);

	bool loadMap(const char* filename, uint16_t firstColId);
	Vec2 getMapSize() const;
	bool moveSprite(Sprite& sprite, const Vec2& amount);	void drawMap();	bool colisionWithTile();	void setSpriteTile(Sprite& sprite) { mSpritesTile.push_back(&sprite); }	void updatePlayer(float deltaTime);	void jump();
	inline std::string extractPath(const std::string& filename) {
		std::string filenameCopy = filename;

		while (filenameCopy.find("\\") != std::string::npos) { 
			filenameCopy.replace(filenameCopy.find("\\"), 1, "/"); 
		}   
		
		filenameCopy = filenameCopy.substr(0, filenameCopy.rfind('/'));   
		
		if (filenameCopy.size() > 0) 
			filenameCopy += "/";

		return filenameCopy;
	}

private:
	std::array<float, 3>		 mColorClear;
	std::array<const ltex_t*, 1> mBacks;
	std::array<float, 1>	     mRatiosScrollBacks;
	std::array<Vec2, 1>			 mVelScrollBacks;
	std::array<Vec2, 1>			 mBacksPos;
	Vec2						 mCameraPos;
	std::vector<Sprite*>		 mListSprites;

	xml_document				 mDoc;
	std::vector<const ltex_t*>   mMap;
	std::vector<int>			 mTilesId;
	int							 mFirstColId;
	std::vector<Sprite*>		 mSpritesTile;
	bool						 mCanJump;
	float						 mSpeedY;
	const float					 mSpeed   = 0.5;
	const float					 mGravity = 12;
	const float					 mJump	  = -4;
	const float					 mMaxAc   = 2;
};