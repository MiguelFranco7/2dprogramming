#include "world.h"

void World::update(float deltaTime) {
	// Actualizar valor del scroll autom�tico en base a la velocidad establecida para cada fondo con setScrollSpeed.
	for (size_t i = 0; i < mBacks.size(); i++) {
		mBacksPos[i].x += mVelScrollBacks[i].x * deltaTime;
		mBacksPos[i].y += mVelScrollBacks[i].y * deltaTime;
	}

	updatePlayer(deltaTime);
	
	// Llamar al m�todo update de cada sprite del mundo.
	for (size_t i = 0; i < mListSprites.size(); i++) {
		mListSprites[i]->update(deltaTime);
	}
}

void World::draw(const Vec2 & screenSize) {
	// Borrado del fondo con el color especificado.
	lgfx_clearcolorbuffer(mColorClear[0], mColorClear[0], mColorClear[2]);
	lgfx_setorigin(0, 0);
	Vec2 mapSize = getMapSize();

	// Actualizado de la camara.
	Vec2 cameraPos = Vec2(mListSprites[0]->getPosition().x - screenSize.x / 2, mListSprites[0]->getPosition().y - screenSize.y / 2);
	if (cameraPos.y < 0)
		cameraPos.y = 0;

	if (cameraPos.y + screenSize.y > mapSize.y)
		cameraPos.y = mapSize.y - screenSize.y;

	if (cameraPos.x < 0)
		cameraPos.x = 0;

	if (cameraPos.x + screenSize.x > mapSize.x)
		cameraPos.x = mapSize.x - screenSize.x;

	// Pintado del fondo.
	for (size_t i = 0; i < mBacks.size(); i++) {
		float stepX = screenSize.x / static_cast<float>(mBacks[i]->width);
		float stepY = screenSize.y / static_cast<float>(mBacks[i]->height);

		float u0 = ((cameraPos.x * mRatiosScrollBacks[i] - mBacksPos[i].x) / static_cast<float>(mBacks[i]->width));
		float v0 = ((cameraPos.y * mRatiosScrollBacks[i] - mBacksPos[i].y) / static_cast<float>(mBacks[i]->height));

		float u1 = u0 + stepX;
		float v1 = v0 + stepY;

		ltex_drawrotsized(mBacks[i], 0, 0, 0, 0, 0, static_cast<float>(screenSize.x), static_cast<float>(screenSize.y), u0, v0, u1, v1);
	}
	
	// Pintar los sprites aplicar la traslaci�n de la c�mara previamente con lgfx_setorigin.
	lgfx_setorigin(cameraPos.x, cameraPos.y);
	setCameraPosition(cameraPos);

	drawMap();

	for (size_t i = 0; i < mListSprites.size(); i++)
		mListSprites[i]->draw();

}

bool World::loadMap(const char * filename, uint16_t firstColId) {
	mFirstColId = firstColId;

	xml_parse_result result = mDoc.load_file(filename);
	if (result) {
		printf("Cargado correctamente el mapa.");
		xml_node mapNode = mDoc.child("map");
		xml_node tileNode = mapNode.child("layer").child("data").child("tile");		for (xml_node tileNode = mapNode.child("layer").child("data").child("tile"); tileNode; tileNode = tileNode.next_sibling("tile")) {
			// Guardamos los ids en el vector de ids de tiles.
			mTilesId.push_back(tileNode.attribute("gid").as_int());
		}

		std::string imageRoute = extractPath(filename) + mapNode.child("tileset").child("image").attribute("source").as_string();
		mMap.push_back(loadTexture(imageRoute.c_str()));
	}
	else
		printf("No se ha podido cargar correctamente el mapa.");

	return false;
}

Vec2 World::getMapSize() const {
	return Vec2(mDoc.child("map").attribute("width").as_float() * mDoc.child("map").attribute("tilewidth").as_float(),
		mDoc.child("map").attribute("height").as_float() * mDoc.child("map").attribute("tileheight").as_float());
}

bool World::moveSprite(Sprite& sprite, const Vec2& amount) {
	Vec2 pos = sprite.getPosition();
	Vec2 nextPos;
	bool isMove = false;

	if (amount.x != 0) {
		nextPos = Vec2(amount.x, 0);
		sprite.setPosition(nextPos + pos);

		if (colisionWithTile())
			sprite.setPosition(pos);
		else
			isMove = true;
	}

	if (amount.y != 0) {
		nextPos = Vec2(0, amount.y);
		sprite.setPosition(nextPos + pos);

		if (colisionWithTile())
			sprite.setPosition(pos);
		else
			isMove = true;
	}
	
	return isMove;
}

void World::drawMap() {
	int numColsImage = mDoc.child("map").child("tileset").attribute("columns").as_int();
	int numRowsImage = mDoc.child("map").child("tileset").attribute("tilecount").as_int() / numColsImage;

	for (size_t i = 0; i < mTilesId.size(); ++i) {
		if (mTilesId[i] > 0) {
			// Obtenemos coordenadas UV
			int row = static_cast<int>(mTilesId[i] - 1) / numColsImage;
			int col = static_cast<int>(mTilesId[i] - 1) % numColsImage;
			float stepU = 1.0f / numColsImage;
			float stepV = 1.0f / numRowsImage;
			float u0 = col * stepU;
			float v0 = row * stepV;
			float u1 = u0 + stepU;
			float v1 = v0 + stepV;

			int rowMap = static_cast<int>(i) / mDoc.child("map").attribute("height").as_int();
			int colMap = static_cast<int>(i) % mDoc.child("map").attribute("width").as_int();

			// Pintamos
			Vec2 tileSize = Vec2(mDoc.child("map").attribute("tilewidth").as_float(), mDoc.child("map").attribute("tileheight").as_float());
			ltex_drawrotsized(mMap[0],
				colMap * tileSize.x, rowMap * tileSize.y,
				0, 0, 0,
				tileSize.x, tileSize.y,
				u0, v0, u1, v1);
		}
	}

}

bool World::colisionWithTile() {
	bool collision = false;
	mSpritesTile[0]->setPosition(Vec2(0, 0));

	for (size_t i = 0; i < mTilesId.size() && !collision; ++i) {
		if (mTilesId[i] >= mFirstColId) {
			int rowMap = static_cast<int>(i) / mDoc.child("map").attribute("height").as_int();
			int colMap = static_cast<int>(i) % mDoc.child("map").attribute("width").as_int();

			mSpritesTile[0]->setPosition(Vec2(colMap * mDoc.child("map").attribute("tilewidth").as_float(), rowMap * mDoc.child("map").attribute("tileheight").as_float()));
			collision = mListSprites[0]->collides(*mSpritesTile[0]);
		}
	}

	return collision;
}

void World::updatePlayer(float deltaTime) {
	mSpeedY += mSpeed * mGravity * deltaTime;

	if (mSpeedY > mSpeed * mMaxAc)
		mSpeedY = mSpeed * mMaxAc;

	if (!moveSprite(*mListSprites[0], Vec2(0, mSpeedY)))
		mCanJump = true;
	else
		mCanJump = false;
}

void World::jump() {
	if (mCanJump) {
		mCanJump = false;
		mSpeedY = mSpeed * mJump;
	}
}
