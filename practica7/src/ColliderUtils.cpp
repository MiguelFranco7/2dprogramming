#include "ColliderUtils.h"
#include <minmax.h>
#include <litegfx.h>

template<class T>
const T clamp(const T& x, const T& upper, const T& lower);

bool checkCircleCircle(const Vec2& pos1, float radius1,
	const Vec2& pos2, float radius2) {
	int dx = pos2.x - pos1.x;
	int dy = pos2.y - pos1.y;
	int rad = radius1 + radius2;

	if ((dx * dx) + (dy * dy) < rad * rad)
		return true;
	else
		return false;
}

bool checkCircleRect(const Vec2& circlePos, float circleRadius,
	const Vec2& rectPos, const Vec2& rectSize) {
	float closestX = clamp(circlePos.x, rectPos.x, rectPos.x + rectSize.x);
	float closestY = clamp(circlePos.y, rectPos.y, rectPos.y + rectSize.y);	float distanceX = circlePos.x - closestX;	float distanceY = circlePos.y - closestY;
	if ((distanceX * distanceX) + (distanceY * distanceY) < circleRadius * circleRadius)
		return true;
	else
		return false;
}

bool checkRectRect(const Vec2& rectPos1, const Vec2& rectSize1,
	const Vec2& rectPos2, const Vec2& rectSize2) {

	if ((max(rectPos1.x, rectPos2.x) < min(rectPos1.x + rectSize1.x, rectPos2.x + rectSize2.x)) &&
		(max(rectPos1.y, rectPos2.y) < min(rectPos1.y + rectSize1.y, rectPos2.y + rectSize2.y))) {
		return true;
	} else return false;
}

bool checkCirclePixels(const Vec2& circlePos, float circleRadius,
	const Vec2& pixelsPos, const Vec2& pixelsSize, const uint8_t* pixels) {
	
	bool colision = false;
	int x0 = max(pixelsPos.x, circlePos.x - circleRadius);
	int x1 = min(pixelsPos.x + pixelsSize.x, circlePos.x + circleRadius);
	int y0 = max(pixelsPos.y, circlePos.y - circleRadius);
	int y1 = min(pixelsPos.y + pixelsSize.y, circlePos.y + circleRadius);

	if (x0 < x1 && y0 < y1) { // CheckRectRect
		for (int row = y0; row < y1 && !colision; ++row) {
			for (int col = x0; col < x1 && !colision; ++col) {

				Vec2 posReal = Vec2(col, row);
				Vec2 pos = posReal - pixelsPos;
				int position = (pos.y * static_cast<int>(pixelsSize.x) + pos.x) * 4 + 3;
				int alpha = pixels[position];

				if (posReal.distance(circlePos) < circleRadius && alpha >= 128) {
					colision = true;
				}
			}
		}
		return colision;
	} else {
		return colision;
	}
}

bool checkPixelsPixels(
	const Vec2& pixelsPos1, const Vec2& pixelsSize1, const uint8_t* pixels1,
	const Vec2& pixelsPos2, const Vec2& pixelsSize2, const uint8_t* pixels2) {

	bool colision = false;
	int x0 = max(pixelsPos1.x, pixelsPos2.x);
	int x1 = min(pixelsPos1.x + pixelsSize1.x, pixelsPos2.x + pixelsSize2.x);
	int y0 = max(pixelsPos1.y, pixelsPos2.y);
	int y1 = min(pixelsPos1.y + pixelsSize1.y, pixelsPos2.y + pixelsSize2.y);

	if (x0 < x1 && y0 < y1) { // CheckRectRect
		for (int row = y0; row < y1 && !colision; ++row) {
			for (int col = x0; col < x1 && !colision; ++col) {

				Vec2 pos1 = Vec2(col, row) - pixelsPos1;
				Vec2 pos2 = Vec2(col, row) - pixelsPos2;

				int position1 = (pos1.y * static_cast<int>(pixelsSize1.x) + pos1.x) * 4 + 3;
				int position2 = (pos2.y * static_cast<int>(pixelsSize2.x) + pos2.x) * 4 + 3;

				int al1 = pixels1[position1];
				int al2 = pixels2[position2];

				if (al1 >= 128 && al2 >= 128) {
					colision = true;
				}
			}
		}
		return colision;
	} else {
		return colision;
	}
}

bool checkPixelsRect(
	const Vec2& pixelsPos, const Vec2& pixelsSize, const uint8_t* pixels,
	const Vec2& rectPos, const Vec2& rectSize) {
	
	bool colision = false;
	int x0 = max(pixelsPos.x, rectPos.x);
	int x1 = min(pixelsPos.x + pixelsSize.x, rectPos.x + rectSize.x);
	int y0 = max(pixelsPos.y, rectPos.y);
	int y1 = min(pixelsPos.y + pixelsSize.y, rectPos.y + rectSize.y);

	if (x0 < x1 && y0 < y1) { // CheckRectRect
		for (int row = y0; row < y1 && !colision; ++row) {
			for (int col = x0; col < x1 && !colision; ++col) {

				Vec2 pos = Vec2(col, row) - pixelsPos;
				int position = (pos.y * static_cast<int>(pixelsSize.x) + pos.x) * 4 + 3;
				int alpha = pixels[position];

				if (alpha >= 128) {
					colision = true;
				}
			}
		}
		return colision;
	} else {
		return colision;
	}
}

template<class T>
const T clamp(const T& val, const T& min, const T& max) {
	return max(min, min(max, val));
}