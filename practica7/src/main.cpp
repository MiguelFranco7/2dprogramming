#ifdef _MSC_VER
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif

#include "world.h"
#include <glfw3.h>
#include <iostream>

using namespace std;

int main() {
	// Inicializamos GLFW
	if (!glfwInit()) {
		cout << "Error: No se ha podido inicializar GLFW" << endl;
		return -1;
	}
	atexit(glfwTerminate);

	// Creamos la ventana
	glfwWindowHint(GLFW_RESIZABLE, true);
	GLFWwindow* window = glfwCreateWindow(800, 600, "Practica 7 Miguel Franco Medina", nullptr, nullptr);
	
	if (!window) {
		cout << "Error: No se ha podido crear la ventana" << endl;
		return -1;
	}

	// Activamos contexto de OpenGL
	glfwMakeContextCurrent(window);

	// Inicializamos LiteGFX
	lgfx_setup2d(800, 600);

	// Cargamos imagenes
	ltex_t* texBackground = loadTexture("data/background.png");
	ltex_t* texPlayer     = loadTexture("data/idle.png");
	ltex_t* texPlayerRun  = loadTexture("data/run.png");
	ltex_t* texTiles	  = loadTexture("data/tiles.png");

	if (!texBackground || !texPlayer || !texPlayerRun || !texTiles) {
		std::cout << "No se ha podido cargar la textura" << std::endl;
		return -1;
	}

	// Creamos el sprite tile
	Sprite spriteTile(texTiles, 8, 5, COLLISION_RECT);

	// Creamos el sprite player
	Sprite spritePlayer(texPlayer, 1, 1, COLLISION_RECT);
	spritePlayer.setFps(4);
	spritePlayer.setPosition(Vec2(40, 860));

	// Creamos el mundo con sus propiedades.
	World world(0.15f, 0.15f, 0.45f, texBackground);
	world.setScrollRatio(0, 1.0f);

	world.addSprite(spritePlayer);
	world.setSpriteTile(spriteTile);

	// Cargamos el mapa.
	world.loadMap("data/map.tmx", 1);

	// Bucle principal
	double lastTime = glfwGetTime();
	while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {
		// Actualizamos delta
		float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
		lastTime = glfwGetTime();

		// Actualizamos tama�o de ventana
		int screenWidth, screenHeight;
		glfwGetWindowSize(window, &screenWidth, &screenHeight);
		lgfx_setviewport(0, 0, screenWidth, screenHeight);
		Vec2 screenSize(static_cast<float>(screenWidth), static_cast<float>(screenHeight));

		// Comprobamos si hay movimiento del player
		if (glfwGetKey(window, GLFW_KEY_RIGHT)) {
			spritePlayer.setFrames(6, 1);
			spritePlayer.setTexture(texPlayerRun);
			spritePlayer.setInverse(false);
			world.moveSprite(spritePlayer, Vec2(150 * deltaTime, 0));
		} else if (glfwGetKey(window, GLFW_KEY_LEFT)) {
			spritePlayer.setFrames(6, 1);
			spritePlayer.setTexture(texPlayerRun);
			spritePlayer.setInverse(true);
			world.moveSprite(spritePlayer, Vec2(-150 * deltaTime, 0));
		} else {
			spritePlayer.setFrames(1, 1);
			spritePlayer.setTexture(texPlayer);
		}

		if (glfwGetKey(window, GLFW_KEY_SPACE)) {
			world.jump();
		}

		// Pintado
		world.update(deltaTime);
		world.draw(Vec2(static_cast<float>(screenWidth), static_cast<float>(screenHeight)));

		// Actualizamos ventana y eventos
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// Eliminamos textura
	ltex_free(texBackground);
	ltex_free(texPlayer);
	ltex_free(texPlayerRun);
	ltex_free(texTiles);

    return 0;
}
