#ifdef _MSC_VER
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif

#define _CRT_SECURE_NO_DEPRECATE
#define LITE_GFX_IMPLEMENTATION
#define STB_TRUETYPE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION

#include <litegfx.h>
#include <stb_truetype.h>
#include <stb_image.h>
#include <glfw3.h>
#include <iostream>
#include <ctime>
#include "stringutils.h"
#include "imageUtils.h"
#include "Vec2.h"
#include "Sprite.h"
#include "Font.h"

using namespace std;

int main() {

	// PRACTICA 4
	// Inicializamos GLFW
	if (!glfwInit()) {
		cout << "Error: No se ha podido inicializar GLFW" << endl;
		return -1;
	}
	atexit(glfwTerminate);

	// Creamos la ventana
	glfwWindowHint(GLFW_RESIZABLE, true);
	GLFWwindow* window = glfwCreateWindow(800, 600, "Practica 4 Miguel Franco Medina", nullptr, nullptr);
	if (!window) {
		cout << "Error: No se ha podido crear la ventana" << endl;
		return -1;
	}

	// Activamos contexto de OpenGL
	glfwMakeContextCurrent(window);

	// Inicializamos LiteGFX
	lgfx_setup2d(800, 600);

	// Definimos variables
	float angle = 0;
	float maxAngle = 15;
	float minAngle = -15;
	float rotFactor = 32;
	float velBee = 128;
	ltex_t* tex = loadTexture("data/bee_anim.png");
	double lastTime = glfwGetTime();
	Sprite *bee = new Sprite(tex, 8, 1);

	// Bucle principal
	while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {

		// Actualizamos delta
		float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
		lastTime = glfwGetTime();

		// Actualizamos tama�o de ventana
		int screenWidth, screenHeight;
		glfwGetWindowSize(window, &screenWidth, &screenHeight);
		lgfx_setviewport(0, 0, screenWidth, screenHeight);

		// Actualizamos coordenadas de raton
		double mouseX, mouseY;
		glfwGetCursorPos(window, &mouseX, &mouseY);
		Vec2 mousePos = Vec2(static_cast<float>(mouseX), static_cast<float>(mouseY));

		// Actualizado de la l�gica del programa
		Vec2 pos = bee->getPosition();
		int dirX = 1;
		int dirY = 1;
		float distanceX = abs(mousePos.x - pos.x);
		float angX = sin(distanceX / mousePos.distance(pos));
		float distanceY = abs(mousePos.y - pos.y);
		float angY = sin(distanceY / mousePos.distance(pos));

		if ((static_cast<float>(mouseX) - pos.x) < 0)
			dirX = -1;
		if ((static_cast<float>(mouseY) - pos.y) < 0)
			dirY = -1;

		if (abs((static_cast<float>(mouseX) - pos.x)) > 1)
			pos.x += dirX * angX * deltaTime * velBee;
		else
			dirX = 0;

		if (abs((static_cast<float>(mouseY) - pos.y)) > 1)
			pos.y += dirY * angY * deltaTime * velBee;
		else
			dirY = 0;

		angle = bee->getAngle();
		if (dirX == -1) {
			angle += rotFactor * deltaTime;
			if (angle >= maxAngle) angle = maxAngle;
			bee->setAngle(angle);
		} else if (dirX == 1) {
			angle -= rotFactor * deltaTime;
			if (angle <= minAngle) angle = minAngle;
			bee->setAngle(angle);
		} else if (dirX == 0 && dirY == 0) {
			if (angle > 0)
				bee->setAngle(angle -= rotFactor * deltaTime);
			else 
				bee->setAngle(angle += rotFactor * deltaTime);
		}

		// Pintado de la abeja.
		lgfx_clearcolorbuffer(0.05f, 0.1f, 0.2f);
		bee->setPosition(pos);
		bee->update(deltaTime);
		bee->draw();

		// Actualizamos ventana y eventos
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	ltex_free(tex);

	// // PRACTICA 3
	//// Inicializamos GLFW
	//if (!glfwInit()) {
	//	cout << "Error: No se ha podido inicializar GLFW" << endl;
	//	return -1;
	//}
	//atexit(glfwTerminate);

	//// Creamos la ventana
	//glfwWindowHint(GLFW_RESIZABLE, true);
	//GLFWwindow* window = glfwCreateWindow(800, 600, "Practica 4 Miguel Franco Medina", nullptr, nullptr);
	//if (!window) {
	//	cout << "Error: No se ha podido crear la ventana" << endl;
	//	return -1;
	//}

	//// Activamos contexto de OpenGL
	//glfwMakeContextCurrent(window);

	//// Inicializamos LiteGFX
	//lgfx_setup2d(800, 600);

	//struct FontControl {
	//	Font *font;
	//	int vel;
	//	Vec2 pos;
	//	float r;
	//	float g;
	//	float b;
	//};

	//std::vector<const char *> ttf;
	//std::vector<FontControl *> fonts;
	//std::vector<Font *> font;

	//ttf.push_back("data/ArmyStamp.ttf");
	//ttf.push_back("data/MICKEY.ttf");
	//ttf.push_back("data/Brainfish.ttf");
	//ttf.push_back("data/SuperMario256.ttf");

	//font.push_back(Font::load(ttf.at(0), rand() % 101));
	//font.push_back(Font::load(ttf.at(1), rand() % 101));
	//font.push_back(Font::load(ttf.at(2), rand() % 101));
	//font.push_back(Font::load(ttf.at(3), rand() % 101));

	//// Bucle principal
	//const char *text = "Hello, world!";
	//double lastTime = glfwGetTime();
	//lgfx_setblend(BLEND_ALPHA);

	//while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {
	//	
	//	// Actualizamos delta
	//	float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
	//	lastTime = glfwGetTime();

	//	// Actualizamos tama�o de ventana
	//	int screenWidth, screenHeight;
	//	glfwGetWindowSize(window, &screenWidth, &screenHeight);
	//	lgfx_setviewport(0, 0, screenWidth, screenHeight);

	//	// Actualizado de la l�gica del programa
	//	if (rand() % 101 < 1) { // Nuevo texto en pantalla.
	//		FontControl *fontC = new FontControl();
	//		fontC->font = font.at(rand() % font.size());
	//		fontC->pos = Vec2(screenWidth, rand() % screenHeight);
	//		fontC->vel = rand() % 201 + 20;
	//		fontC->r = rand() % 2;
	//		fontC->g = rand() % 2;
	//		fontC->b = rand() % 2;

	//		fonts.push_back(fontC);
	//	}

	//	/*for (auto o : list) {}
	//	std::remove_if() usando lamb*/
	//	lgfx_clearcolorbuffer(0, 0, 0);
	//	for (auto it = fonts.begin(); it != fonts.end();) {
	//		lgfx_setcolor((*it)->r, (*it)->g, (*it)->b, 1);
	//		(*it)->font->draw(text, (*it)->pos);
	//		(*it)->pos.x -= deltaTime * (*it)->vel;
	//		
	//		if ((*it)->pos.x < (*it)->font->texture->width*-1) {
	//			delete (*it);
	//			it = fonts.erase(it);
	//		} else {
	//			it++;
	//		}
	//	}

	//	// Actualizamos ventana y eventos
	//	glfwSwapBuffers(window);
	//	glfwPollEvents();
	//}

	// PRACTICA 2
	//// Inicializamos GLFW
	//if (!glfwInit()) {
	//	cout << "Error: No se ha podido inicializar GLFW" << endl;
	//	return -1;
	//}
	//atexit(glfwTerminate);

	//// Creamos la ventana
	//glfwWindowHint(GLFW_RESIZABLE, true);
	//GLFWwindow* window = glfwCreateWindow(800, 600, "Practica 2 Miguel Franco Medina", nullptr, nullptr);
	//if (!window) {
	//	cout << "Error: No se ha podido crear la ventana" << endl;
	//	return -1;
	//}

	//// Activamos contexto de OpenGL
	//glfwMakeContextCurrent(window);

	//// Inicializamos LiteGFX
	//lgfx_setup2d(800, 600);

	//// Bucle principal
	//float angle = 0;
	//float velAngle = 10;
	//float escalado = 1; // 0.8 - 1.2 a 0.5 por seg.
	//float velEscalado = 0.5;
	//double lastTime = glfwGetTime();
	//int operAngle = 1;
	//int operEsc = 1;
	// TODO: Hacer el load de las imagenes aqui.

	//while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {

	//	// Actualizamos delta
	//	float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
	//	lastTime = glfwGetTime();

	//	// Actualizamos tama�o de ventana
	//	int screenWidth, screenHeight;
	//	glfwGetWindowSize(window, &screenWidth, &screenHeight);
	//	lgfx_setviewport(0, 0, screenWidth, screenHeight);

	//	// Actualizamos coordenadas de raton
	//	double mouseX, mouseY;
	//	glfwGetCursorPos(window, &mouseX, &mouseY);

	// TODO: Ajustar angulo y rotacion al limite para que no se salga
	//	// Actualizado de la l�gica del programa
	//	if (operAngle == 1) angle += velAngle * deltaTime;
	//	else angle -= velAngle * deltaTime;

	//	if (angle >= 10) operAngle = -1;
	//	else if (angle <= -10) operAngle = 1;

	//	if (operEsc == 1) escalado += velEscalado * deltaTime;
	//	else escalado -= velEscalado * deltaTime;

	//	if (escalado >= 1.2) operEsc = -1;
	//	else if (escalado <= 0.8) operEsc = 1;

	//	// Pintado
	//	lgfx_setblend(BLEND_SOLID);
	//	ltex_t* tex = loadTexture("data/wall.jpg");
	//	ltex_drawrotsized(tex, 0, 0, 0, 0, 0, static_cast<float>(screenWidth), static_cast<float>(screenHeight), 0, 0, static_cast<float>(screenWidth) / static_cast<float>(tex->width), static_cast<float>(screenHeight) / static_cast<float>(tex->height));
	//	
	//	float pivotX = 0.5f;
	//	float pivotY = 0.7f;
	//	lgfx_setblend(BLEND_ADD);
	//	tex = loadTexture("data/fire.png");
	//	ltex_drawrotsized(tex, static_cast<float>(mouseX), static_cast<float>(mouseY), angle, pivotX, pivotY, tex->width*escalado, tex->height*escalado, 0, 0, 1, 1);

	//	lgfx_setblend(BLEND_ALPHA);
	//	tex = loadTexture("data/grille.png");
	//	ltex_drawrotsized(tex, 0, 0, 0, 0, 0, static_cast<float>(screenWidth), static_cast<float>(screenHeight), 0, 0, static_cast<float>(screenWidth) / static_cast<float>(tex->width), static_cast<float>(screenHeight) / static_cast<float>(tex->height));

	//	lgfx_setblend(BLEND_MUL);
	//	tex = loadTexture("data/light.png");
	//	ltex_drawrotsized(tex, static_cast<float>(mouseX - tex->width / 2), static_cast<float>(mouseY - tex->height / 2), 0, 0, 0, static_cast<float>(tex->width), static_cast<float>(tex->height), 0, 0, 1, 1);

	//	lgfx_setcolor(0, 0, 0, 0);
	//	lgfx_drawrect(0, 0, static_cast<float>(screenWidth), static_cast<float>(mouseY - tex->height / 2));
	//	lgfx_drawrect(0, 0, static_cast<float>(mouseX - tex->height / 2), static_cast<float>(screenHeight));
	//	lgfx_drawrect(static_cast<float>(mouseX + tex->height / 2), 0, static_cast<float>(screenWidth - mouseX + tex->width / 2), static_cast<float>(screenHeight));
	//	lgfx_drawrect(0, static_cast<float>(mouseY + tex->height / 2), static_cast<float>(screenWidth), static_cast<float>(screenHeight - mouseY + tex->height/ 2));
	//	lgfx_setcolor(1, 1, 1, 1);

	//	// Actualizamos ventana y eventos
	//	glfwSwapBuffers(window);
	//	glfwPollEvents();

	//	deleteTexture(tex);
	//}

	// PRACTICA 1
	//// Inicializamos GLFW
	//if (!glfwInit()) {
	//	cout << "Error: No se ha podido inicializar GLFW" << endl;
	//	return -1;
	//}
	//atexit(glfwTerminate);

	//// Creamos la ventana
	//glfwWindowHint(GLFW_RESIZABLE, true);
	//GLFWwindow* window = glfwCreateWindow(800, 600, "Practica 1", nullptr, nullptr);
	//if (!window) {
	//	cout << "Error: No se ha podido crear la ventana" << endl;
	//	return -1;
	//}

	//// Activamos contexto de OpenGL
	//glfwMakeContextCurrent(window);

	//// Inicializamos LiteGFX
	//lgfx_setup2d(800, 600);

	//// Bucle principal
	//float angle = 0;
	//double lastTime = glfwGetTime();
	//while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {
	//	// Actualizamos delta
	//	float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
	//	lastTime = glfwGetTime();

	//	// Actualizamos tama�o de ventana
	//	int screenWidth, screenHeight;
	//	glfwGetWindowSize(window, &screenWidth, &screenHeight);
	//	lgfx_setviewport(0, 0, screenWidth, screenHeight);

	//	// Actualizamos coordenadas de raton
	//	double mouseX, mouseY;
	//	glfwGetCursorPos(window, &mouseX, &mouseY);

	//	// Actualizacion de logica del programa
	//	angle += 32 * deltaTime;
	//	float radAngle = angle * DEG2RAD;
	//	Vec2 size(32, 32);
	//	Vec2 center = Vec2(static_cast<float>(screenWidth), static_cast<float>(screenHeight)) / 2;
	//	Vec2 mousePos(static_cast<float>(mouseX), static_cast<float>(mouseY));
	//	Vec2 circle = mousePos + Vec2(cos(radAngle), -sin(radAngle)) * 64;
	//	Vec2 halfSize = size / 2;
	//	Vec2 centerOrig = center - halfSize;
	//	Vec2 mouseOrig = mousePos - halfSize;
	//	Vec2 circleOrig = circle - halfSize;

	//	// Pintado
	//	lgfx_clearcolorbuffer(1, 1, 1);
	//	lgfx_setcolor(1, 0, 0, 1);
	//	lgfx_drawrect(centerOrig.x, centerOrig.y, size.x, size.y);
	//	lgfx_setcolor(0, 1, 0, 1);
	//	lgfx_drawrect(mouseOrig.x, mouseOrig.y, size.x, size.y);
	//	lgfx_setcolor(0, 0, 1, 1);
	//	lgfx_drawoval(circleOrig.x, circleOrig.y, size.x, size.y);

	//	// Actualizamos ventana
	//	std::string title = "Distancia: "
	//		+ stringFromNumber<int>(mousePos.distance(center))
	//		+ " -- Angulo: "
	//		+ stringFromNumber<int>(mousePos.angle(circle));
	//	glfwSetWindowTitle(window, title.c_str());

	//	// Actualizamos ventana y eventos
	//	glfwSwapBuffers(window);
	//	glfwPollEvents();
	//}

    return 0;
}