#include <litegfx.h>
#include <stb_truetype.h>
#include "Vec2.h"
#include "Sprite.h"

Sprite::Sprite(const ltex_t* tex, int hframes, int vframes) {
	texture = tex;
	hFrames = hframes;
	vFrames = vframes;
	blendMode = BLEND_ALPHA;
	r = 1;
	g = 1;
	b = 1;
	alpha = 1;
	pos.x = 1;
	pos.y = 1;
	angle = 0;
	setScale(Vec2(1, 1));
	setPivot(Vec2(0.5f, 0.5f));
	currentFrame = 0;
	fps = 8;
}

const ltex_t* Sprite::getTexture() const {
	return texture;
}

void Sprite::setTexture(const ltex_t* tex) {
	texture = tex;
}

lblend_t Sprite::getBlend() const {
	return blendMode;
}

void Sprite::setBlend(lblend_t mode) {
	blendMode = mode;
}

float Sprite::getRed() const {
	return r;
}

float Sprite::getGreen() const {
	return g;
}

float Sprite::getBlue() const {
	return b;
}

float Sprite::getAlpha() const {
	return alpha;
}

void Sprite::setColor(float red, float green, float blue, float a) {
	r = red;
	g = green;
	b = blue;
	alpha = a;
}

const Vec2& Sprite::getPosition() const {
	return pos;
}

void Sprite::setPosition(const Vec2& position) {
	pos = position;
}

float Sprite::getAngle() const {
	return angle;
}

void Sprite::setAngle(float ang) {
	angle = ang;
}

const Vec2& Sprite::getScale() const {
	return scale;
}

void Sprite::setScale(const Vec2& sca) {
	scale = sca;
}

// Tama�o de un frame multiplicado por la escala
Vec2 Sprite::getSize() const {
	Vec2 vec;
	vec.x = scale.x * (texture->width / hFrames);
	vec.y = scale.y * (texture->height / vFrames);
	return vec;
}

// Este valor se pasa a ltex_drawrotsized en el pintado
// para indicar el pivote de rotaci�n
const Vec2& Sprite::getPivot() const {
	return pivot;
}

void Sprite::setPivot(const Vec2& piv) {
	pivot = piv;
}

int Sprite::getHframes() const {
	return hFrames;
}

int Sprite::getVframes() const {
	return vFrames;
}
void Sprite::setFrames(int hframes, int vframes) {
	hFrames = hframes;
	vFrames = vframes;
}

// Veces por segundo que se cambia el frame de animaci�n
int Sprite::getFps() const {
	return fps;
}

void Sprite::setFps(int f) {
	fps = f;
}

// Frame actual de animaci�n
float Sprite::getCurrentFrame() const {
	return currentFrame;
}

void Sprite::setCurrentFrame(int frame) {
	currentFrame = static_cast<float>(frame);
}

void Sprite::update(float deltaTime) {
	// Actualizar frame seg�n los fps.
	// Comprobar si nos pasamos para volver al inicio.
	currentFrame += fps * deltaTime;

	if (currentFrame > hFrames * vFrames)
		currentFrame = 0;
}

void Sprite::draw() const {
	// Calcular coordenadas UV seg�n el currentFrame.
	// Establecer propiedades de pintado (Blend, color).
	// Dibujar textura con ltex_drawrotsized calculando tama�o seg�n escala y rotacion adecuada.
	Vec2 size = getSize();
	float u0 = (static_cast<int>(currentFrame) % hFrames) * (1.0f / hFrames);
	float u1 = (static_cast<int>(currentFrame) % hFrames) * (1.0f / hFrames) + 1.0f / hFrames;

	float v0 = static_cast<int>(currentFrame / hFrames) * (1.0f / vFrames);
	float v1 = static_cast<int>(currentFrame / hFrames) * (1.0f / vFrames) + (1.0f / vFrames);
	
	lgfx_setblend(getBlend());
	lgfx_setcolor(r, g, b, alpha);
	ltex_drawrotsized(texture, pos.x, pos.y, angle, pivot.x, pivot.y, size.x, size.y, u0, v0, u1, v1);
}

Sprite::~Sprite() {

}