#ifdef _MSC_VER
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif

#define LITE_GFX_IMPLEMENTATION
#include <litegfx.h>
#include <glfw3.h>
#include <iostream>
#include <sstream>
#include "CVec2.h"

using namespace std;

const float sizeRect     = 100;
const float sizeOval     = 70;
const int   widthWindow  = 800;
const int   heightWindow = 600;

template <typename T>
string stringFromNumber(T val);

int main() {
	// Inicializamos GLFW
	if (!glfwInit()) {
		cout << "Error: No se ha podido inicializar GLFW" << endl;
		return -1;
	}
	atexit(glfwTerminate);

	// Creamos la ventana
	glfwWindowHint(GLFW_RESIZABLE, false);
	GLFWwindow* window = glfwCreateWindow(widthWindow, heightWindow, "Programacion 2D", nullptr, nullptr);
	if (!window) {
		cout << "Error: No se ha podido crear la ventana" << endl;
		return -1;
	}

	// Actualizamos tama�o de ventana
	int screenWidth, screenHeight;
	glfwGetWindowSize(window, &screenWidth, &screenHeight);
	lgfx_setviewport(0, 0, screenWidth, screenHeight);

	// Activamos contexto de OpenGL
	glfwMakeContextCurrent(window);

	// Inicializamos LiteGFX
	lgfx_setup2d(widthWindow, heightWindow);

	// Bucle principal
	CVec2  rect      = CVec2(static_cast<float>(screenWidth / 2), static_cast<float>(screenHeight / 2));
	float  degree    = 0;
	double lastTime  = glfwGetTime();
	double xPosMouse = 0;
	double yPosMouse = 0;
	float  xPosOval  = 0;
	float  yPosOval  = 0;
	float  deltaTime;
	CVec2  mouse;
	CVec2  oval;
	string title;

	while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {
		// Actualizamos delta
		deltaTime = static_cast<float>(glfwGetTime() - lastTime);
		lastTime  = glfwGetTime();

		// Actualizacion de logica del programa
		glfwGetCursorPos(window, &xPosMouse, &yPosMouse);
		// Calcula la posicion del oval.
		degree  -= 32 * deltaTime;
		xPosOval = static_cast<float>(xPosMouse-35 + cos(degree*PI/180) * 150);
		yPosOval = static_cast<float>(yPosMouse-35 + sin(degree*PI/180) * 150);

		// Pintado.
		// Cuadrado central.
		lgfx_clearcolorbuffer(1, 1, 1);
		lgfx_setcolor(0, 1, 1, 1);
		lgfx_drawrect(rect.getX() - 50, rect.getY() - 50, sizeRect, sizeRect);
		// Cuadrado del cursor.
		lgfx_setcolor(1, 0, 1, 1);
		lgfx_drawrect(static_cast<float>(xPosMouse-50), static_cast<float>(yPosMouse-50), sizeRect, sizeRect);
		// Ovalo que da vueltas.
		lgfx_setcolor(0, 0, 1, 1);
		lgfx_drawoval(xPosOval, yPosOval, sizeOval, sizeOval);

		// Actualizamos ventana y eventos
		glfwSwapBuffers(window);
		glfwPollEvents();

		mouse = CVec2(static_cast<float>(xPosMouse), static_cast<float>(yPosMouse));
		oval  = CVec2(xPosOval, yPosOval);
		title = "Distancia: "
			+ stringFromNumber(rect.distance(mouse))
			+ " -- Angulo: "
			+ stringFromNumber(mouse.angle(oval));
		glfwSetWindowTitle(window, title.c_str());
	}

    return 0;
}

template <typename T>
string stringFromNumber(T val) {
	ostringstream stream;
	stream << fixed << val;
	return stream.str();
}