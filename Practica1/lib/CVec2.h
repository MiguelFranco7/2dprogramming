#define PI 3.14159265

class CVec2 {
public:
	CVec2                 ();
	CVec2                 (float x, float y);

	void suma             (const CVec2 &vec);
	void suma             (float esc);
	void resta            (const CVec2 &vec);
	void resta            (float esc);
	void producto         (const CVec2 &vec);
	void producto         (float esc);
	void division         (const CVec2 &vec);
	void division         (float esc);

	CVec2 valorAbsoluto   ();
	float longitud        ();
	CVec2 normal          ();
	float productoEscalar (const CVec2 &vec);

	float getX            ();
	float getY            ();

	float CVec2::angle    (const CVec2& other) const;
	float CVec2::distance (const CVec2& other) const;

	~CVec2                ();

private:
	float x;
	float y;
};
