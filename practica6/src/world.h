#pragma once
#include "vec2.h"
#include "sprite.h"
#include <litegfx.h>
#include <vector>

class World {
public:
	World(
		float clearRed = 0.15f, float clearGreen = 0.15f, float clearBlue = 0.15f,
		const ltex_t* back0 = nullptr, const ltex_t* back1 = nullptr,
		const ltex_t* back2 = nullptr, const ltex_t* back3 = nullptr) :
		mColorClear{ clearRed, clearGreen, clearBlue },
		mBacks{ back0, back1, back2, back3 },
		mRatiosScrollBacks{ 0, 0, 0, 0 },
		mVelScrollBacks{ 0, 0, 0, 0 },
		mBacksPos{0, 0, 0, 0},
		mCameraPos(0, 0) {}

	~World() { // Delete sprites.
		for (size_t i = 0; i < mListSprites.size(); i++) {
			mListSprites.erase(mListSprites.begin() + i);
		}
	}

	float getClearRed	() const		   { return mColorClear[0]; }
	void  setClearRed	(float clearRed)   { mColorClear[0] = clearRed; }
	float getClearGreen () const		   { return mColorClear[1]; }
	void  setClearGreen (float clearGreen) { mColorClear[1] = clearGreen; }
	float getClearBlue  () const		   { return mColorClear[2]; }
	void  setClearBlue  (float clearBlue)  { mColorClear[2] = clearBlue; }

	const ltex_t* getBackground(size_t layer) const { return mBacks[layer]; }
	void setBackground(size_t layer, ltex_t* back) { mBacks[layer] = back; }

	float getScrollRatio(size_t layer) const { return mRatiosScrollBacks[layer]; }
	void setScrollRatio(size_t layer, float ratio) { mRatiosScrollBacks[layer] = ratio; }

	const Vec2& getScrollSpeed(size_t layer) const { return mVelScrollBacks[layer]; }
	void setScrollSpeed(size_t layer, const Vec2& speed) { mVelScrollBacks[layer] = speed; }

	const Vec2& getCameraPosition() const { return mCameraPos; }
	void setCameraPosition(const Vec2& pos) { mCameraPos = pos; }

	void addSprite(Sprite& sprite) { mListSprites.push_back(&sprite); }
	void removeSprite(Sprite& sprite) {
		for (size_t i = 0; i <= mListSprites.size(); i++) {
			if (&sprite == mListSprites[i]) {
				mListSprites.erase(mListSprites.begin() + i);
			}
		}
	}
	
	void update(float deltaTime);
	void draw(const Vec2& screenSize);

private:
	std::array<float, 3>		 mColorClear;
	std::array<const ltex_t*, 4> mBacks;
	std::array<float, 4>	     mRatiosScrollBacks;
	std::array<Vec2, 4>			 mVelScrollBacks;
	std::array<Vec2, 4>			 mBacksPos;
	Vec2						 mCameraPos;
	std::vector<Sprite*>		 mListSprites;
};