#ifdef _MSC_VER
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif

#define _CRT_SECURE_NO_DEPRECATE
#define LITE_GFX_IMPLEMENTATION
#define STB_TRUETYPE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION

#include <litegfx.h>
#include <stb_truetype.h>
#include <stb_image.h>
#include <glfw3.h>
#include <iostream>
#include "stringutils.h"
#include "imageUtils.h"
#include "Vec2.h"
#include "Sprite.h"
#include "Font.h"
#include "Collider.h"

using namespace std;

int main() {

	// PRACTICA 5
	// Inicializamos GLFW
	if (!glfwInit()) {
		cout << "Error: No se ha podido inicializar GLFW" << endl;
		return -1;
	}
	atexit(glfwTerminate);

	// Creamos la ventana
	glfwWindowHint(GLFW_RESIZABLE, true);
	GLFWwindow* window = glfwCreateWindow(800, 600, "Practica 5 Miguel Franco Medina", nullptr, nullptr);
	if (!window) {
		cout << "Error: No se ha podido crear la ventana" << endl;
		return -1;
	}

	// Activamos contexto de OpenGL
	glfwMakeContextCurrent(window);

	// Inicializamos LiteGFX
	lgfx_setup2d(800, 600);

	// Definimos variables
	Vec2 mousePos;

	float   scaleFactor = 0.25;
	int     dirScale	= 1;
	float   maxScale    = 1.1f;
	float   minScale    = 0.9f;
	ltex_t* texBall     = loadTexture("data/ball.png");
	ltex_t* texBee      = loadTexture("data/bee.png");
	ltex_t* texBox      = loadTexture("data/box.png");
	ltex_t* texCircle   = loadTexture("data/circle.png");
	ltex_t* texRect     = loadTexture("data/rect.png");
	
	Sprite* ball        = new Sprite(texBall, 1, 1, COLLISION_CIRCLE);
	Sprite* bee         = new Sprite(texBee, 1, 1, COLLISION_PIXELS);
	Sprite* box         = new Sprite(texBox, 1, 1, COLLISION_RECT);
	Sprite* mouseSprite = new Sprite(texCircle, 1, 1, COLLISION_CIRCLE);

	double lastTime = glfwGetTime();

	// Bucle principal
	while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {

		// Actualizamos delta
		float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
		lastTime = glfwGetTime();

		// Actualizamos tama�o de ventana
		int screenWidth, screenHeight;
		glfwGetWindowSize(window, &screenWidth, &screenHeight);
		lgfx_setviewport(0, 0, screenWidth, screenHeight);

		// Actualizamos coordenadas de raton
		double mouseX, mouseY;
		glfwGetCursorPos(window, &mouseX, &mouseY);
		mousePos = Vec2(static_cast<float>(mouseX), static_cast<float>(mouseY));

		// Comprueba si se pulsa un boton del raton.
		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)) {
			mouseSprite->setTexture(texCircle);
			mouseSprite->setCollisionType(COLLISION_CIRCLE);
		} else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT)) {
			mouseSprite->setTexture(texRect);
			mouseSprite->setCollisionType(COLLISION_RECT);
		} else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE)) {
			mouseSprite->setTexture(texBee);
			mouseSprite->setCollisionType(COLLISION_PIXELS);
		}

		// Actualizado de la l�gica del programa.
		// Actualizado la escala de los sprites (la misma en ball y box).
		mouseSprite->setPosition(mousePos);
		Vec2 scaleBall = ball->getScale();

		scaleBall.x += dirScale * scaleFactor * deltaTime;
		scaleBall.y += dirScale * scaleFactor * deltaTime;
		if (scaleBall.x >= maxScale || scaleBall.y >= maxScale) {
			scaleBall.x = maxScale;
			scaleBall.y = maxScale;
			dirScale = -1;
		} else if (scaleBall.x <= minScale || scaleBall.y <= minScale) {
			scaleBall.x = minScale;
			scaleBall.y = minScale;
			dirScale = 1;
		}
		
		ball->setScale(scaleBall);
		box->setScale(scaleBall);

		// Comprobar colisiones y establecer colores.
		bool mouseCollision = false;

		// Ball
		if (ball->collides(*mouseSprite)) {
			ball->setColor(1, 0, 0, 1);
			mouseCollision = true;
		} else
			ball->setColor(1, 1, 1, 1);

		// Boll
		if (box->collides(*mouseSprite)) {
			box->setColor(1, 0, 0, 1);
			mouseCollision = true;
		} else
			box->setColor(1, 1, 1, 1);

		// Bee
		if (bee->collides(*mouseSprite)) {
			bee->setColor(1, 0, 0, 1);
			mouseCollision = true;
		} else
			bee->setColor(1, 1, 1, 1);

		// Acualizado de color del sprite de mouse.
		if (mouseCollision)
			mouseSprite->setColor(1, 0, 0, 1);
		else
			mouseSprite->setColor(1, 1, 1, 1);

		// Pintado de los Sprites.
		float positionXElements = static_cast<float>(screenWidth / 4);
		float positionYElements = static_cast<float>(screenHeight / 2);
		lgfx_clearcolorbuffer(0.7f, 0.7f, 0.7f);

		ball->setPosition(Vec2(positionXElements, positionYElements));
		bee->setPosition(Vec2(positionXElements * 2, positionYElements));
		box->setPosition(Vec2(positionXElements * 3, positionYElements));
		ball->draw();
		bee->draw();
		box->draw();
		mouseSprite->draw();

		// Actualizamos ventana y eventos
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	ltex_free(texBall);
	ltex_free(texBee);
	ltex_free(texBox);
	ltex_free(texCircle);
	ltex_free(texRect);

    return 0;
}