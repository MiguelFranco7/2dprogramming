#include "RectCollider.h"
#include "ColliderUtils.h"

RectCollider::RectCollider(Vec2 *pos, Vec2 siz, Vec2 *sca, Vec2 *piv) {
	position = pos;
	size = siz;
	scale = sca;
	pivot = piv;
}

bool RectCollider::collides(const Collider& other) const {
	return other.collides(getPositionCollider(), getSizeCollider());
}

bool RectCollider::collides(const Vec2& circlePos, float circleRadius) const {
	return checkCircleRect(circlePos, circleRadius, getPositionCollider(), getSizeCollider());
}

bool RectCollider::collides(const Vec2& rectPos, const Vec2& rectSize) const {
	return checkRectRect(getPositionCollider(), getSizeCollider(), rectPos, rectSize);
}

bool RectCollider::collides(const Vec2& pixelsPos, const Vec2& pixelsSize, const uint8_t* pixels) const {
	return checkPixelsRect(pixelsPos, pixelsSize, pixels, getPositionCollider(), getSizeCollider());
}

Vec2 RectCollider::getSizeCollider() const {
	return Vec2(size.x * scale->x, size.y * scale->y);
}

Vec2 RectCollider::getPositionCollider() const {
	return Vec2(position->x - (pivot->x * size.x * scale->x), position->y - (pivot->y * size.y * scale->y));
}