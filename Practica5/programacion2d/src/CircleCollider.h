#pragma once
#include <iostream>
#include "Collider.h"
#include "Vec2.h"

class CircleCollider : public Collider {
public:
	CircleCollider(Vec2 *position, Vec2 size, Vec2 *scale);
	virtual bool collides(const Collider& other) const;
	virtual bool collides(const Vec2& circlePos, float circleRadius) const;
	virtual bool collides(const Vec2& rectPos, const Vec2& rectSize) const;
	virtual bool collides(const Vec2& pixelsPos, const Vec2& pixelsSize,
		const uint8_t* pixels) const;

	Vec2 *position;
	Vec2  size;
	Vec2 *scale;

private:
	float getRadius() const;
};
