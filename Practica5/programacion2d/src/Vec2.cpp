//#include "CVec2.h"
//#include <cmath>
//
//CVec2::CVec2(float x, float y) {
//	this->x = x;
//	this->y = y;
//}
//
//void CVec2::suma(const CVec2 &vec) {
//	x += vec.x;
//	y += vec.y;
//}
//
//void CVec2::suma(float esc) {
//	x += esc;
//	y += esc;
//}
//
//void CVec2::resta(const CVec2 &vec) {
//	x -= vec.x;
//	y -= vec.y;
//}
//
//void CVec2::resta(float esc) {
//	x -= esc;
//	y -= esc;
//}
//
//void CVec2::producto(const CVec2 &vec) {
//	x *= vec.x;
//	y *= vec.y;
//}
//
//void CVec2::producto(float esc) {
//	x *= esc;
//	y *= esc;
//}
//
//void CVec2::division(const CVec2 &vec) {
//	x /= vec.x;
//	y /= vec.y;
//}
//
//void CVec2::division(float esc) {
//	x /= esc;
//	y /= esc;
//}
//
//CVec2 CVec2::valorAbsoluto() {
//	CVec2 ret;
//	ret.x = static_cast<float>(fabs(x));
//	ret.y = static_cast<float>(fabs(y));
//	return ret;
//}
//
//float CVec2::longitud() {
//	return sqrt(x*x + y*y);
//}
//
//CVec2 CVec2::normal() {
//	CVec2 ret;
//	float magnitud = static_cast<float>(sqrt((x*x) + (y*y)));
//
//	if (magnitud != 0) {
//		ret.x = x / magnitud;
//		ret.y = y / magnitud;
//	}
//
//	return ret;
//}
//
//float CVec2::productoEscalar(const CVec2 &vec) {
//	return x * vec.x + y * vec.y;
//}
//
//float CVec2::getX() {
//	return x;
//}
//
//float CVec2::getY() {
//	return y;
//}
//
//float CVec2::angle(const CVec2& other) const {
//	float angle = static_cast<float>(atan2(other.y - y, other.x - x) * 180 / -PI); // atan2(y - other.y, other.x - x) * RAD2DEG
//
//	if (angle < 0)
//		angle = angle + 360;
//
//	return angle;
//}
//
//float CVec2::distance(const CVec2& other) const {
//	float a = other.x - x;
//	float b = other.y - y;
//
//	return static_cast<float>(sqrt((a*a + b*b)));
//}