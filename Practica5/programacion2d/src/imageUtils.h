#pragma once

ltex_t* loadTexture(const char* filename) {
	int x;
	int y;
	// Carga de ficheros de imagen.
	unsigned char *loadImage = stbi_load(filename, &x, &y, nullptr, 4);

	if (!loadImage) return nullptr;

	// Paso 1: Generaci�n de la textura.
	ltex_t* tex = ltex_alloc(x, y, 0);

	// Paso 2: Volcado de los p�xeles.
	ltex_setpixels(tex, loadImage);

	// Borramos buffer de STB Image.
	stbi_image_free(loadImage);

	return tex;
}

void deleteTexture(ltex_t *text) {
	ltex_free(text);
}