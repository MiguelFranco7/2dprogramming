#pragma once
#include "Collider.h"
#include <vector>

using namespace std;

enum CollisionType {
	COLLISION_NONE,
	COLLISION_CIRCLE,
	COLLISION_RECT,
	COLLISION_PIXELS
};

struct Sprite {
public:
	typedef void(*CallbackFunc)(Sprite&, float);

	// Indicamos el n�mero de frames en horizontal y vertical
	// que tendr� la imagen del sprite
	Sprite(const ltex_t* tex, int hframes = 1, int vframes = 1);
	Sprite(const ltex_t* tex, int hframes = 1, int vframes = 1, CollisionType type = COLLISION_NONE);

	const ltex_t* getTexture() const;
	void setTexture(const ltex_t* tex);

	lblend_t getBlend() const;
	void setBlend(lblend_t mode);

	float getRed() const;
	float getGreen() const;
	float getBlue() const;
	float getAlpha() const;
	void setColor(float r, float g, float b, float a);

	const Vec2& getPosition() const;
	void setPosition(const Vec2& pos);

	float getAngle() const;
	void setAngle(float angle);

	const Vec2& getScale() const;
	void setScale(const Vec2& scale);

	// Tama�o de un frame multiplicado por la escala
	Vec2 getSize() const;

	// Este valor se pasa a ltex_drawrotsized en el pintado
	// para indicar el pivote de rotaci�n
	const Vec2& getPivot() const;
	void setPivot(const Vec2& pivot);

	int getHframes() const;
	int getVframes() const;
	void setFrames(int hframes, int vframes);

	// Veces por segundo que se cambia el frame de animaci�n
	int getFps() const;
	void setFps(int fps);

	// Frame actual de animaci�n
	float getCurrentFrame() const;
	void setCurrentFrame(int frame);

	void update(float deltaTime);
	void draw() const;

	CallbackFunc getCallback() { return mCallback; }
	void setCallback(CallbackFunc callback) { mCallback = callback; }

	void* getUserData() { return mUserData; }
	void setUserData(void* data) { mUserData = data; }

	void setCollisionType(CollisionType type);
	CollisionType getCollisionType() const;
	const Collider* getCollider() const;
	bool collides(const Sprite& other) const;

	~Sprite();

private:
	const ltex_t* texture;
	int hFrames;
	int vFrames;
	lblend_t blendMode;
	float r;
	float g;
	float b;
	float alpha;
	Vec2 pos;
	float angle;
	Vec2 scale;
	Vec2 pivot;
	int fps;
	float currentFrame;
	CallbackFunc			mCallback;
	void*					mUserData;

	vector<uint8_t> pixels;
	CollisionType collisionType;
	Collider *collider;
	Vec2 rectColPos;
	Vec2 rectColSize;
	float radius;
};