#include "PixelsCollider.h"
#include "ColliderUtils.h"

PixelsCollider::PixelsCollider(Vec2 *pos, Vec2 siz, Vec2 *piv, uint8_t *pix) {
	position = pos;
	size = siz;
	pivot = piv;
	pixels = pix;
}

bool PixelsCollider::collides(const Collider& other) const {
	return other.collides(getPositionCollider(), size, pixels);
}

bool PixelsCollider::collides(const Vec2& circlePos, float circleRadius) const {
	return checkCirclePixels(circlePos, circleRadius, getPositionCollider(), size, pixels);
}

bool PixelsCollider::collides(const Vec2& rectPos, const Vec2& rectSize) const {
	return checkPixelsRect(getPositionCollider(), size, pixels, rectPos, rectSize);
}

bool PixelsCollider::collides(const Vec2& pixelsPos, const Vec2& pixelsSize, const uint8_t* pixelsBuf) const {
	return checkPixelsPixels(getPositionCollider(), size, pixels, pixelsPos, pixelsSize, pixelsBuf);
}

Vec2 PixelsCollider::getPositionCollider() const {
	return Vec2(position->x - (pivot->x * size.x), position->y - (pivot->y * size.y));
}