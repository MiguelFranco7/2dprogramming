#include <litegfx.h>
#include "Vec2.h"
#include "Sprite.h"
#include "CircleCollider.h"
#include "RectCollider.h"
#include "PixelsCollider.h"

Sprite::Sprite(const ltex_t* tex, int hframes, int vframes) : 
	texture(tex),
	hFrames(hframes),
	vFrames(vframes),
	blendMode(BLEND_ALPHA),
	r(1),
	g(1),
	b(1),
	alpha(1),
	pos(1, 1),
	angle(0),
	scale(1, 1),
	pivot(0.5f, 0.5f),
	fps(0),
	currentFrame(0) {}

Sprite::Sprite(const ltex_t* tex, int hframes, int vframes, CollisionType type) :
	texture(tex),
	hFrames(hframes),
	vFrames(vframes),
	blendMode(BLEND_ALPHA),
	r(1),
	g(1),
	b(1),
	alpha(1),
	pos(1, 1),
	angle(0),
	scale(1, 1),
	pivot(0.5f, 0.5f),
	fps(0),
	currentFrame(0),
	collider(nullptr) {
	setCollisionType(type);
}

const ltex_t* Sprite::getTexture() const {
	return texture;
}

void Sprite::setTexture(const ltex_t* tex) {
	texture = tex;
}

lblend_t Sprite::getBlend() const {
	return blendMode;
}

void Sprite::setBlend(lblend_t mode) {
	blendMode = mode;
}

float Sprite::getRed() const {
	return r;
}

float Sprite::getGreen() const {
	return g;
}

float Sprite::getBlue() const {
	return b;
}

float Sprite::getAlpha() const {
	return alpha;
}

void Sprite::setColor(float red, float green, float blue, float a) {
	r = red;
	g = green;
	b = blue;
	alpha = a;
}

const Vec2& Sprite::getPosition() const {
	return pos;
}

void Sprite::setPosition(const Vec2& position) {
	pos = position;
}

float Sprite::getAngle() const {
	return angle;
}

void Sprite::setAngle(float ang) {
	angle = ang;
}

const Vec2& Sprite::getScale() const {
	return scale;
}

void Sprite::setScale(const Vec2& sca) {
	scale = sca;
}

// Tama�o de un frame multiplicado por la escala
Vec2 Sprite::getSize() const {
	Vec2 vec;
	vec.x = scale.x * (texture->width / hFrames);
	vec.y = scale.y * (texture->height / vFrames);
	return vec;
}

// Este valor se pasa a ltex_drawrotsized en el pintado
// para indicar el pivote de rotaci�n
const Vec2& Sprite::getPivot() const {
	return pivot;
}

void Sprite::setPivot(const Vec2& piv) {
	pivot = piv;
}

int Sprite::getHframes() const {
	return hFrames;
}

int Sprite::getVframes() const {
	return vFrames;
}
void Sprite::setFrames(int hframes, int vframes) {
	hFrames = hframes;
	vFrames = vframes;
}

// Veces por segundo que se cambia el frame de animaci�n
int Sprite::getFps() const {
	return fps;
}

void Sprite::setFps(int f) {
	fps = f;
}

// Frame actual de animaci�n
float Sprite::getCurrentFrame() const {
	return currentFrame;
}

void Sprite::setCurrentFrame(int frame) {
	currentFrame = static_cast<float>(frame);
}

void Sprite::update(float deltaTime) {
	// Actualizar frame seg�n los fps.
	// Comprobar si nos pasamos para volver al inicio.
	currentFrame += fps * deltaTime;

	if (currentFrame > hFrames * vFrames)
		currentFrame = 0;

	// Llamamos callback
	if (mCallback) mCallback(*this, deltaTime);

	// Actualizamos animacion
	int totalFrames = hFrames * vFrames;
	currentFrame += fps * deltaTime;
	if (currentFrame >= totalFrames) currentFrame -= totalFrames;
	if (currentFrame < 0) currentFrame += totalFrames;
}

void Sprite::draw() const {
	// Obtenemos coordenadas UV
	int row = static_cast<int>(currentFrame) / hFrames;
	int col = static_cast<int>(currentFrame) % hFrames;
	float stepU = 1.0f / hFrames;
	float stepV = 1.0f / vFrames;
	float u0 = col * stepU;
	float v0 = row * stepV;
	float u1 = u0 + stepU;
	float v1 = v0 + stepV;

	// Establecemos propiedades de pintado
	lgfx_setblend(blendMode);
	lgfx_setcolor(r, g, b, alpha);

	// Pintamos
	Vec2 size = getSize();
	ltex_drawrotsized(texture,
		pos.x, pos.y,
		angle, pivot.x, pivot.y,
		size.x, size.y,
		u0, v0, u1, v1);
}

void Sprite::setCollisionType(CollisionType type) {
	collisionType = type;
	delete collider;

	switch (collisionType) {
	case COLLISION_CIRCLE: 
		collider = new CircleCollider(&pos, Vec2(static_cast<float>(texture->width / 2), static_cast<float>(texture->height / 2)), &scale);
		break;
	case COLLISION_RECT: 
		collider = new RectCollider(&pos, Vec2(static_cast<float>(texture->width), static_cast<float>(texture->height)), &scale, &pivot);
		break;
	case COLLISION_PIXELS:
		pixels.resize(static_cast<const unsigned int>(texture->width * texture->height * 4));
		ltex_getpixels(texture, pixels.data());
		collider = new PixelsCollider(&pos, Vec2(static_cast<float>(texture->width), static_cast<float>(texture->height)), &pivot, pixels.data());
		break;
	case COLLISION_NONE: 
		collider = nullptr;
		break;
	}
}

CollisionType Sprite::getCollisionType() const {
	return collisionType;
}

const Collider* Sprite::getCollider() const {
	return collider;
}

bool Sprite::collides(const Sprite& other) const {
	const Collider *c = other.getCollider();
	if (collider && c) {
		return collider->collides(*c);
	} else 
		return false;
}

Sprite::~Sprite() {

}